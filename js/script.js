const btns = document.querySelectorAll('.btn');
// console.log(btns);

window.addEventListener('keydown', function(e) {
    for (let i = 0; i < btns.length; i++) {
        if (e.code === btns[i].dataset.code) {
            btns[i].style.backgroundColor = '#0000ff';
        } else {
            btns[i].style.backgroundColor = '#000000';
        }
    }
})